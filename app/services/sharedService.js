module.exports = function sharedService(){
	var sharedService = {};

	sharedService.pages = {}

	sharedService.Turmas = [];
	sharedService.Turma = [];
	sharedService.acceptTermsStatus = false;

	sharedService.resizeBase64Img = function(base64, img) {
	    var canvas = document.createElement("canvas");
	 	var context = canvas.getContext("2d");
	    
	    var deferred = $.Deferred();

	    $(img != null ? "#" + img : "<img/>").attr("src",  base64).on('load', function() {
		  	var MAXWidthHeight = 800;
			var r = MAXWidthHeight / Math.max(this.width, this.height);
			var w = Math.round(this.width * r);
			var h = Math.round(this.height * r);
			
			canvas.width = w;
		    canvas.height = h;
	    
	        //context.scale(w/this.width,  h/this.height);
	        context.drawImage(this, 0, 0, w, h); 
	        deferred.resolve(canvas.toDataURL());    
		                 
		});
	    
	    return deferred.promise();    
	}

	return sharedService;
}
