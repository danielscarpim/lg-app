module.exports = function dataFactory($http,$filter,loadingService){
    //var server = 'http://www.lgacademy.com.br/WebService/'; // Produção
    var server = 'http://177.47.23.157:10009/WebService/';  // Dev
    var dataFactory = {};
    var ambiente = "Dev"; // Mudar para 'Dev' quando for testar. Deixar em branco para produção.

    /*********** AVALIACOES INICIO **************/

    dataFactory.getAvaliacoesRespondidas = function(tipoAvaliacao){
        loadingService.startLoading();
        return $http.get(server + 'Avaliacao.ashx?Method=ListAvaliacoesPorUsuario'+ ambiente +'&tipoAvaliacao=' + tipoAvaliacao);
    }

    dataFactory.getListaQuestoesRespondidas = function(idAvaliacaoRespondida){
        loadingService.startLoading();
        return $http.get(server + 'Avaliacao.ashx?Method=GetAvaliacoesRespondidas&idAvaliacaoRespondida=' + idAvaliacaoRespondida);
    }

    dataFactory.getNovaAvaliacaoReacao = function(idTurma){
        loadingService.startLoading();
        return $http.get(server + 'Avaliacao.ashx?Method=NovaAvaliacaoReacao&idTurma=' + idTurma);
    }

    dataFactory.getNovaAvaliacao = function(idAvaliacao, idTurma, tipoAvaliacao){
        loadingService.startLoading();
        return $http.get(server + 'Avaliacao.ashx?Method=NovaAvaliacao'+ ambiente +'&idAvaliacao=' + idAvaliacao + '&idTurma=' + idTurma + '&tipoAvaliacao=' + tipoAvaliacao);
    }

    dataFactory.putNovaAvaliacao = function(avRespondida){
        return $http({
            url: server + 'NovaAvaliacao.ashx',
            method: "POST",
            data: {avRespondida}
        });
    }

    dataFactory.putNovaAvaliacaoReacao = function(avrRespondida){
        return $http({
            url: server + 'AvaliacaoReacao.ashx',
            method: "POST",
            data: {avrRespondida}
        });
    }

    dataFactory.getTermoAceiteAvaliacao = function(){
        return $http.get(server + 'Avaliacao.ashx?Method=VerificaTermoAceiteAvaliacao');
    }

    dataFactory.putTermoAceiteAvaliacao = function(hasAccepted){
         $http.get(server + 'Avaliacao.ashx?Method=AtualizaTermoAceiteAvaliacao&hasAccepted=' + hasAccepted).then(function(response){
            if(response.data != null && response.data == true)
                return true;
        });
    }

    /*********** AVALIACOES FIM **************/


    /*********** MAIN INICIO *****************/

    dataFactory.getUltimasNoticias = function(){
        loadingService.isLoading = true;
        return $http.get(server + 'Noticia.ashx?Method=UltimasNoticias' + ambiente);
    }

    dataFactory.getDetalheNoticia = function(id){
        loadingService.isLoading = true;
        return $http.get(server + 'Noticia.ashx?Method=GetNoticia' + ambiente + '&id=' + id);
    }

    dataFactory.getTurmasCalendario = function(date){
        loadingService.isLoading = true;
        return $http.get(server + 'Turma.ashx?Method=GetTurmasCalendarioByInstrutorByMes' + ambiente + '&date=' + date);
    }

        dataFactory.getTurmaCalendario = function(id){
        loadingService.isLoading = true;
        return $http.get(server + 'Turma.ashx?Method=GetTurmaCalendarioById&id=' + id);
    }

    dataFactory.getResumoDia = function(date){
        loadingService.isLoading = true;
        return $http.get(server + 'GeoPosition.ashx?Method=GetKilometragemPosition' + ambiente + '&date=' + date);
    }

    dataFactory.getTurmasCalendarioByInstrutorByDia = function(date){
        loadingService.isLoading = true;
        return $http.get(server + 'Turma.ashx?Method=GetTurmasCalendarioByInstrutorByDia' + ambiente + '&date=' + date);
    }

    /***************** INICIO EVIDENCIAS *****************/

    dataFactory.getEvidencias = function(idTurma){
        loadingService.isLoading = true;
        return $http.get(server + 'Evidencia.ashx?Method=ListEvidencia' + ambiente + '&idTurma=' + idTurma);
    }

    dataFactory.putNewEvidencia = function(file){
        loadingService.isLoading = true;
        return $http({
            url: server + 'GetPosition.asmx/UploadEvidencia',
            data : {'upload': angular.toJson({ 'idTurma': file.id, 'data': file.data, 'fileName': file.name }) },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            method: "POST"
        });
    }

    dataFactory.deleteEvidencia = function(idTurma, path){
        loadingService.isLoading = true;
        return $http.get(server + 'GetPosition.asmx/DeleteEvidencia?idTurma=' + idTurma + '&path=' + path);
    }

    /***************** FIM EVIDENCIAS *****************/

    /***************** INICIO CUSTOS *****************/    

    dataFactory.getCusto = function(idCusto){
        loadingService.isLoading = true;
        return $http.get(server + 'Custos.ashx?Method=GetCusto&id=' + idCusto)
    }

    dataFactory.getCustosByTurma = function(idTurma){
        loadingService.isLoading = true;
        return $http.get(server + 'Custos.ashx?Method=ListCustos' + ambiente + '&idTurma=' + idTurma);
    }

    dataFactory.getListCustosSumario = function(date){
        loadingService.isLoading = true;
        return $http.get(server + 'Custos.ashx?Method=GetListCustosSumario' + ambiente + '&date=' + date);
    }

    dataFactory.getCustos = function(idTurma){
        loadingService.isLoading = true;
        return $http.get(server + 'custos.ashx?Method=ListCustosInstrutor' + ambiente + '&idTurma=' + idTurma);
    }

    dataFactory.getTiposCusto = function(){
        loadingService.isLoading = true;
        return $http.get(server + 'custos.ashx?Method=GetTipoCusto');
    }

    dataFactory.deleteCusto = function(idTurma, idCusto){
        loadingService.isLoading = true;
        return $http.get(server + 'custos.ashx?Method=DeleteCusto&idTurma=' + idTurma + "&idCusto=" + idCusto);
    }

    dataFactory.vinculaCusto = function(idTurma, idCusto){
        loadingService.isLoading = true;
        return $http.get(server + 'custos.ashx?Method=VincularCusto&idTurma=' + idTurma + '&idCusto=' + idCusto);
    }

    dataFactory.uploadRecibo = function(upload){
        loadingService.isLoading = true;
        return $http({
            url: server + 'GetPosition.asmx/UploadRecibo',
            data : {'upload': angular.toJson(upload) },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            method: "POST"
        });
    }

    dataFactory.salvaNovoCusto = function(custo){
        loadingService.isLoading = true;
        
        return $http({
            url: server + 'GetPosition.asmx/UploadNewRecibo',
            data : {'upload': angular.toJson(custo) },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            method: "POST"
        });
    }

    dataFactory.DeleteRecibo = function(idCusto, file){
        loadingService.isLoading = true;
        return $http.get(server + 'custos.ashx?Method=DeleteRecibo&idCusto=' + idCusto + "&file=" + file);
    }

    /***************** FIM CUSTOS *****************/    

    /*********** MAIN FIM *****************/

    return dataFactory;
}
