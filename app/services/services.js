import dataFactory from './dataFactory.js';
import loadingService from './loadingService.js';
import sharedService from './sharedService.js';

angular
  .module('services',[])
  .factory('dataFactory', dataFactory)
  .service('loadingService', loadingService)
  .service('sharedService', sharedService)
