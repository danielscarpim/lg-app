
// Módulos Instrutor
import mainInstrutor from './components/main/instrutor/main.js';
import agenda from './components/agenda/agenda.js';
import noticias from './components/noticias/noticias.js';

// Módulos Promotor
import mainPromotor from './components/main/promotor/main.js';
import avaliacoes from './components/avaliacoes/avaliacoes.js';
import treinamentos from './components/treinamentos/treinamentos.js';
import biblioteca from './components/biblioteca/biblioteca.js';
//
// // Common helpers
import services from './services/services.js';
import filters from './filters/filters.js';
//
// // Styles
import './scss/app.scss';

var commonModules = [
                      'ui.router',
                      'ngAnimate',
                      'ngSanitize',
                      'ngComboDatePicker',
                      'ui.utils.masks',
                      'mm.foundation',
                      'services',
                      'filters'
                    ];

var instrutorModules = ['main.instrutor','agenda','noticias'];

var promotorModules = ['main.promotor','avaliacoes','treinamentos','biblioteca'];

// init angular app
angular
  .module('lg-app-instrutor',[
    ...commonModules,
    ...instrutorModules
  ]);

angular
  .module('lg-app-promotor',[
    ...commonModules,
    ...promotorModules
  ])
