// Libraries
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import fullcalendar from 'fullcalendar';
import gcal from 'fullcalendar/dist/gcal.js';
import uiCalendar from 'angular-ui-calendar';
import 'angular-i18n/angular-locale_pt-br.js';
import ngComboDatePicker from 'ng-combo-date-picker';
import inputMasks from 'angular-input-masks';
import angularFoundation6 from 'angular-foundation-6';
