module.exports = function capitalizeFirstLetter(){
    return function(token) {
        return token.charAt(0).toUpperCase() + token.slice(1);
    };
}
