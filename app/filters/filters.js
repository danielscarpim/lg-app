import toDate from './toDate.js';
import capitalizeFirstLetter from './capitalizeFirstLetter';
import titlecase from './titlecase.js';

angular
  .module('filters',[])
  .filter('toDate', toDate)
  .filter('capitalizeFirstLetter', capitalizeFirstLetter)
  .filter('titlecase', titlecase)
