import template from 'ngtemplate!html!./agenda.html';
import agendaController from './agendaController.js';
import subMenu from '../../components/subMenu/subMenu.js';

import calendario from '../../components/calendario/calendario.js';
import turmas from '../../components/turmas/turmas.js';

angular
  .module('agenda',['calendario','turmas','subMenu'])
  .config(agendaRoutes);

  function agendaRoutes($stateProvider,$urlRouterProvider){
    $urlRouterProvider.when('/main/agenda', '/main/agenda/calendario');
    $stateProvider
      .state('main.agenda',{
        url: "/agenda",
        views: {
          "tabs-main": {
            templateUrl: template,
            controller: agendaController
          },
        }
      })
  }
