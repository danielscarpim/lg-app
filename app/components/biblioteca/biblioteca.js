import template from 'ngtemplate!html!./biblioteca.html';
import bibliotecaController from './bibliotecaController.js';


angular
  .module('biblioteca',[])
  .config(bibliotecaRoutes);

  function bibliotecaRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('main.biblioteca',{
        url: "/biblioteca",
        views: {
          "tabs-main": {
            templateUrl: template,
            controller: bibliotecaController,
            controllerAs: 'biblioteca'
          },
        }
      })
  }
