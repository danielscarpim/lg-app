module.exports = function bibliotecaController($scope){
  var biblioteca = this;
      biblioteca.list = [
          	{
          		"title": "LG news 07/2016",
          		"type": "Imagem",
              "filetype": "png",
              "icon": "ion-image",
          	},{
          		"title": "Video Lançamento Home Theater",
          		"type": "Video",
              "filetype": "mp4",
              "icon": "ion-social-youtube",
          	},{
          		"title": "Folder All in one PC",
          		"type": "Publicidade",
              "filetype": "pdf",
              "icon": "ion-document-text",
          	},{
              "title": "Video Treinamento Ar Condicionado",
          		"type": "Video",
              "filetype": "mp4",
              "icon": "ion-social-youtube",
          	},{
          		"title": "Manual LG G4",
          		"type": "Manual",
              "filetype": "pdf",
              "icon": "ion-document-text",
          	}
          ];
}
