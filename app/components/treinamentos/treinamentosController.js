module.exports = function treinamentosController($scope){
  var treinamentos = this;
      treinamentos.list = [
          	{
          		"title": "Treinamento Home Theater 2016",
          		"type": "Home Theater - CAV",
              "icon": "ion-social-youtube",
          		"image": require('../../images/home-theater.png')
          	},
          	{
          		"title": "Aplicativo LG Music Flow Bluetooth",
          		"type": "Conceitual - CAV",
              "icon": "ion-monitor",
          		"image": require('../../images/music-flow.png')
          	},
          	{
          		"title": "Mini System linha 9000",
          		"type": "Mini System - CAV",
              "icon": "ion-document-text",
          		"image": require('../../images/mini-system.png')
          	},
          	{
          		"title": "Guia de Shop Display - HE",
          		"type": "Shop Display",
              "icon": "ion-social-youtube",
          		"image": require('../../images/guia-shop.png')
          	},
          	{
          		"title": "LG All - In - One PC",
          		"type": "Computadores Pessoais",
              "icon": "ion-monitor",
          		"image": require('../../images/all-in-one-pc.png')
          	}
          ];
}
