import template from 'ngtemplate!html!./treinamentos.html';
import treinamentosController from './treinamentosController.js';


angular
  .module('treinamentos',[])
  .config(treinamentosRoutes);

  function treinamentosRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('main.treinamentos',{
        url: "/treinamentos",
        views: {
          "tabs-main": {
            templateUrl: template,
            controller: treinamentosController,
            controllerAs: 'treinamentos'
          },
        }
      })
  }
