import template from 'ngtemplate!html!./avaliacoes.html';
import tipo from 'ngtemplate!html!./avaliacoes.tipo.html';
import avaliacoesController from './avaliacoesController.js';
import subMenu from '../../components/subMenu/subMenu.js';
import pageHeader from '../../components/pageHeader/pageHeader.js';
import avaliacao from './avaliacao/avaliacao.js';
import avaliacaoReacao from './reacao/avaliacaoReacao.js';
import acceptTerms from '../../components/acceptTerms/acceptTerms.js';

angular
  .module('avaliacoes',['pageHeader','subMenu','avaliacao','avaliacaoReacao','acceptTerms'])
  .config(avaliacoesRoutes);

  function avaliacoesRoutes($stateProvider,$urlRouterProvider){
    $urlRouterProvider.when('/main/avaliacoes/', '/main/avaliacoes/PRE');
    $stateProvider
      .state('main.avaliacoes',{
        url: "/avaliacoes/:tipoAvaliacao",
        data: {
          title: "Avaliações Respondidas"
        },
        views: {
          "tabs-main": {
            templateUrl: template,
            controller: avaliacoesController,
            controllerAs: 'avaliacoes'
          },
          "tabs-avaliacoes@main.avaliacoes": {
            templateUrl: tipo
          }
        }
      })
  }
