import template from 'ngtemplate!html!./resultadoAvaliacaoReacao.html';
import resultadoAvaliacaoReacaoController from './resultadoAvaliacaoReacaoController.js';

angular
  .module('resultadoAvaliacaoReacao',[])
  .config(resultadoAvaliacaoReacaoRoutes);

  function resultadoAvaliacaoReacaoRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('resultadoAvaliacaoReacao',{
        url: "/resultadoAvaliacaoReacao/:status",
        views: {
          "main": {
            templateUrl: template,
            controller: resultadoAvaliacaoReacaoController,
            controllerAs: 'rar'
          }
        }
      })
  }
