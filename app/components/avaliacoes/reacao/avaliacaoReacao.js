import novaAvaliacaoReacao from './nova/novaAvaliacaoReacao.js';
import resultadoAvaliacaoReacao from './resultado/resultadoAvaliacaoReacao.js';

angular
  .module('avaliacaoReacao',['novaAvaliacaoReacao','resultadoAvaliacaoReacao'])
