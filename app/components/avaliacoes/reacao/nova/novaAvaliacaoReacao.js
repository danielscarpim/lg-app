import template from 'ngtemplate!html!./novaAvaliacaoReacao.html';
import novaAvaliacaoReacaoController from './novaAvaliacaoReacaoController.js';

angular
  .module('novaAvaliacaoReacao',[])
  .config(novaAvaliacaoReacaoRoutes)

  function novaAvaliacaoReacaoRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('novaAvaliacaoReacao',{
        url: "/novaAvaliacaoReacao/:idTurma",
        views: {
          "main": {
            templateUrl: template,
            controller: novaAvaliacaoReacaoController,
            controllerAs: 'nar'
          }
        }
      })
  }
