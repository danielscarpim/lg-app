﻿module.exports = function novaAvaliacaoReacaoController($state,$scope,dataFactory,$stateParams,$document,loadingService){

    var nar = this;
        nar.Reacao = {};
        nar.Reacao.TotalQuestoes = 0;

    var idTurma = $stateParams.idTurma;

    var objNovaAvaliacaoReacaoRespondida = {};
    objNovaAvaliacaoReacaoRespondida.IDAvaliacaoReacao = 0;
    objNovaAvaliacaoReacaoRespondida.CodigoAvaliacaoReacao = 0;
    objNovaAvaliacaoReacaoRespondida.IDTurma = 0;
    objNovaAvaliacaoReacaoRespondida.Selecionadas = [];

    dataFactory.getNovaAvaliacaoReacao(idTurma).then(function(response){
        nar.Reacao = response.data;

        objNovaAvaliacaoReacaoRespondida.IDAvaliacaoReacao = nar.Reacao.ID;
        objNovaAvaliacaoReacaoRespondida.CodigoAvaliacaoReacao = nar.Reacao.CodigoAvaliacao;
        objNovaAvaliacaoReacaoRespondida.IDTurma = idTurma;

        nar.Reacao.TotalQuestoes = nar.Reacao.Questoes.length;
        nar.Reacao.TotalRespondidas = 0;
        nar.Reacao.QuestaoRespondida = [];

        loadingService.isLoading = false;
    });

    nar.bindAvaliacaoReacaoRespondida = function(idQuestao, idAlternativa){
        let obj = {}
            obj.IDQuestao = idQuestao;
            obj.IDAlternativa = idAlternativa;

        //Filtro por idQuestão
        var result = $.grep(objNovaAvaliacaoReacaoRespondida.Selecionadas, function(e){ return e.IDQuestao == idQuestao });

        if(result.length <= 0)
        {
            objNovaAvaliacaoReacaoRespondida.Selecionadas.push(obj);
        }
    }

    nar.fillBar = function(index, questao, alternativa){
        nar.bindAvaliacaoReacaoRespondida(questao.ID, alternativa.ID);

        var question = document.querySelector('#question-' + questao.ID);
        $(question).addClass('selected');

        var pm = document.querySelector('#progress-meter-' + questao.ID);

        var ulOptions = document.querySelector('#ulOptions-' + questao.ID);
        $(ulOptions).removeClass("option0 option1 option2 option3 option4");
        $(ulOptions).addClass("option" + index);

        $(pm).css('option' + index);

        switch (index) {
            case 0:
                $(pm).css( "width", "0%" );
                break;
            case 1:
                $(pm).css( "width", "25%" );
                break;
            case 2:
                $(pm).css( "width", "50%" );
                break;
            case 3:
                $(pm).css( "width", "75%" );
                break;
            case 4:
                $(pm).css( "width", "100%" );
                break;
            default:

        }

        nar.preencheProgressBar(questao.ID);

    }

    nar.SubmitAvaliacaoReacao = function(){
      dataFactory.putNovaAvaliacaoReacao(objNovaAvaliacaoReacaoRespondida).then(function(response){
          $state.go('resultadoAvaliacaoReacao', { 'status' : response.data.result });
      });
    }

    nar.preencheProgressBar = function(idQuestao){
        var answered = false;

        if(nar.Reacao.QuestaoRespondida.length == 0) {
            nar.Reacao.QuestaoRespondida.push(idQuestao);
        }
        else{
            if(nar.Reacao.QuestaoRespondida.indexOf(idQuestao) >= 0){
                answered = true;
            }
            else{
                answered = false;
                nar.Reacao.QuestaoRespondida.push(idQuestao);
            }
        }

        if(!answered && nar.Reacao.TotalRespondidas == 0) nar.Reacao.TotalRespondidas = 1;
        else if(!answered)
        {
            nar.Reacao.TotalRespondidas++;
        }

        nar.Reacao.Percentual = (nar.Reacao.TotalRespondidas / nar.Reacao.TotalQuestoes ) * 100;

    }

}
