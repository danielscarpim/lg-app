﻿module.exports = function novaAvaliacaoController($state,$scope,dataFactory,loadingService,$stateParams){
    var na = this;
        na.Avaliacao = {};
        na.ProgressBarAvaliacao = null;
        na.Avaliacao.TotalQuestoes = 0;
        na.Avaliacao.FinalizaProgressBar = true;

    var objNovaAvaliacaoRespondida = {};
        objNovaAvaliacaoRespondida.IDAvaliacao = 0;
        objNovaAvaliacaoRespondida.IDTurma = 0;
        objNovaAvaliacaoRespondida.tipoAvaliacao = '';
        objNovaAvaliacaoRespondida.Selecionadas = [];

    var idAvaliacao = $stateParams.idAvaliacao;
    var idTurma = $stateParams.idTurma;
    var tipoAvaliacao = $stateParams.tipoAvaliacao;



    dataFactory.getNovaAvaliacao(idAvaliacao, idTurma, tipoAvaliacao).then(function (response){

        na.Avaliacao = response.data;

        objNovaAvaliacaoRespondida.IDTurma = idTurma;
        objNovaAvaliacaoRespondida.IDUsuario = response.data.IDUsuario;

        if(response.data.JaRespondida != true){
            na.Avaliacao.TotalQuestoes = na.Avaliacao.Questoes.length;
            na.Avaliacao.TotalRespondidas = 0;
            na.Avaliacao.QuestaoRespondida = [];
        }
        loadingService.isLoading = false;
    });


    na.bindAvaliacaoRespondida = function(idAvaliacao, idQuestao, idAlternativa){
        objNovaAvaliacaoRespondida.IDAvaliacao = idAvaliacao;
        objNovaAvaliacaoRespondida.tipoAvaliacao = tipoAvaliacao;

        let obj = {}
            obj.IDQuestao = idQuestao;
            obj.IDAlternativa = idAlternativa;

        //Filtro por idQuestão
        var result = $.grep(objNovaAvaliacaoRespondida.Selecionadas, function(e){ return e.IDQuestao == idQuestao });

        if(result.length <= 0)
        {
            objNovaAvaliacaoRespondida.Selecionadas.push(obj);
        }
    }

    na.preencheProgressBar = function(idQuestao){
        var answered = false;

        var question = document.querySelector('#question-' + idQuestao);
            $(question).addClass('selected');

        if(na.Avaliacao.QuestaoRespondida.length == 0) {
            na.Avaliacao.QuestaoRespondida.push(idQuestao);
        }
        else{
            if(na.Avaliacao.QuestaoRespondida.indexOf(idQuestao) >= 0){
                answered = true;
            }
            else{
                answered = false;
                na.Avaliacao.QuestaoRespondida.push(idQuestao);
            }
        }

        if(!answered && na.Avaliacao.TotalRespondidas == 0) na.Avaliacao.TotalRespondidas = 1;
        else if(!answered)
        {
            na.Avaliacao.TotalRespondidas++;
        }

        na.Avaliacao.Percentual = (na.Avaliacao.TotalRespondidas / na.Avaliacao.TotalQuestoes ) * 100;
    }

    na.ProgressAvaliacao = function(idAvaliacao, idQuestao, idAlternativa){
        na.bindAvaliacaoRespondida(idAvaliacao, idQuestao, idAlternativa);
        na.preencheProgressBar(idQuestao);
    }

    na.SubmitAvaliacao = function(){
      dataFactory.putNovaAvaliacao(objNovaAvaliacaoRespondida).then(function(response){
          if(response.data.jaRespondida) na.Avaliacao.JaRespondida = true;
          else {
              $state.go('resultadoAvaliacao',  {  "idAvaliacaoRespondida" : response.data.idAvaliacaoRespondida,
                                                  "notaFinal"             : response.data.nota,
                                                  "qtdTotalQuestoes"      : response.data.qtdQuestoes,
                                                  "aprovado"              : response.data.aprovado,
                                                  "qtdQuestoesCorretas"   : response.data.qtdQuestoesCorretas,
                                                  "tipoAvaliacao"		      : response.data.tipoAvaliacao
              });
          }
      });
    }
}
