import template from 'ngtemplate!html!./novaAvaliacao.html';
import novaAvaliacaoController from './novaAvaliacaoController.js';

angular
  .module('novaAvaliacao',[])
  .config(novaAvaliacaoRoutes);

  function novaAvaliacaoRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('novaAvaliacao',{
        url: "/novaAvaliacao/:idAvaliacao/:idTurma/:tipoAvaliacao",
        views: {
          "main": {
            templateUrl: template,
            controller: novaAvaliacaoController,
            controllerAs: 'na'
          }
        }
      })
  }
