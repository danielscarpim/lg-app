import novaAvaliacao from './nova/novaAvaliacao.js';
import avaliacaoRespondida from './respondida/avaliacaoRespondida.js';
import resultadoAvaliacao from './resultado/resultadoAvaliacao.js';

angular
  .module('avaliacao',['avaliacaoRespondida','novaAvaliacao','resultadoAvaliacao'])
