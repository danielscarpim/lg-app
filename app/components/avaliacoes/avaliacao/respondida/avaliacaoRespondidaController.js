module.exports = function avaliacaoRespondidaController($state,$scope,dataFactory,loadingService,$stateParams){
    var qr = this;
        qr.result = {};
        qr.ID = $stateParams.idAvaliacaoRespondida;

   dataFactory.getListaQuestoesRespondidas(qr.ID).then(function (response){
       qr.result = response.data;
       loadingService.isLoading = false;
   });
}
