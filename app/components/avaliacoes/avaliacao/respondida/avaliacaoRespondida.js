import template from 'ngtemplate!html!./avaliacaoRespondida.html';
import avaliacaoRespondidaController from './avaliacaoRespondidaController.js';

angular
  .module('avaliacaoRespondida',[])
  .config(avaliacaoRespondidaRoutes);

  function avaliacaoRespondidaRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('avaliacaoRespondida',{
        url: "/avaliacaoRespondida/:idAvaliacaoRespondida",
        data: {
          title: "Avaliação Respondidas"
        },
        views: {
          "main": {
            templateUrl: template,
            controller: avaliacaoRespondidaController,
            controllerAs: 'qr'
          }
        }
      })
  }
