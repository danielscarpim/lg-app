import template from 'ngtemplate!html!./resultadoAvaliacao.html';
import resultadoAvaliacaoController from './resultadoAvaliacaoController.js';

angular
  .module('resultadoAvaliacao',[])
  .config(resultadoAvaliacaoRoutes);

  function resultadoAvaliacaoRoutes($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('resultadoAvaliacao',{
        url: "/resultadoAvaliacao/:idAvaliacaoRespondida/:notaFinal/:qtdTotalQuestoes/:aprovado/:qtdQuestoesCorretas",
        data: {
          title: "Avaliação Finalizada",
          back: "main.avaliacoes"
        },
        views: {
          "main": {
            templateUrl: template,
            controller: resultadoAvaliacaoController,
            controllerAs: 'ra'
          }
        }
      })
  }
