module.exports = function resultadoAvaliacaoController($state,$scope,dataFactory,loadingService,$stateParams){
    var ra = this;
        ra.Result = {}
        ra.Result.idAvaliacaoRespondida = $stateParams.idAvaliacaoRespondida;
        ra.Result.nota = $stateParams.notaFinal;
        ra.Result.qtdQuestoes = $stateParams.qtdTotalQuestoes;
        ra.Result.aprovado = $stateParams.aprovado;
        ra.Result.qtdQuestoesCorretas = $stateParams.qtdQuestoesCorretas;

    loadingService.isLoading = false;
}
