module.exports = function avaliacoesController($state,$scope,dataFactory,loadingService,$stateParams){
    var avaliacoes = this;
        avaliacoes.avaliacoesRespondidas = {};
        avaliacoes.currentView = $state.current.title;
        avaliacoes.tipo = $stateParams.tipoAvaliacao;

    if(avaliacoes.tipo != undefined && avaliacoes.tipo != '' && avaliacoes.tipo != null) {
      dataFactory.getAvaliacoesRespondidas(avaliacoes.tipo).then(function (response){
          avaliacoes.avaliacoesRespondidas = response.data;
          loadingService.isLoading = false;
      })
    }

}
