import template from 'ngtemplate!html!./loader.html';
import loaderController from './loaderController.js';

angular
  .module('loader', [])
  .component('loader',{
    templateUrl: template,
    controller: loaderController
  })
