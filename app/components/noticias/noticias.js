import template from 'ngtemplate!html!./noticias.html';
import noticiasController from './noticiasController.js';
import pageHeader from '../../components/pageHeader/pageHeader.js';
import detalheNoticia from './detalheNoticia/detalheNoticia.js';

angular
  .module('noticias',['pageHeader','detalheNoticia'])
  .config(noticiasRoutes);

  function noticiasRoutes($stateProvider){
    $stateProvider
      .state('main.noticias',{
        url: "/noticias",
        data: {
          title: "Últimas Notícias"
        },
        views: {
          "tabs-main": {
            templateUrl: template,
            controller: noticiasController,
            controllerAs: 'noticias'
          },
        }
      })
  }
