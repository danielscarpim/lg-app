import template from 'ngtemplate!html!./detalheNoticia.html';
import detalheNoticiaController from './detalheNoticiaController.js';

angular
  .module('detalheNoticia',[])
  .config(detalheNoticiaRoutes);

  function detalheNoticiaRoutes($stateProvider){
    $stateProvider
      .state('detalheNoticia',{
        url: "/detalheNoticia/:id",
        data: {
          title: "Notícias",
          back: 'main.noticias',
        },
        views: {
          "main": {
            templateUrl: template,
            controller: detalheNoticiaController,
            controllerAs: 'noticia'
          },
        }
      })
  }
