module.exports = function detalheNoticiaController($scope,dataFactory,$stateParams,loadingService){
  var noticia = this;

  noticia.Detalhe = {};

  noticia.Detalhe.ID = $stateParams.id;

  dataFactory.getDetalheNoticia(noticia.Detalhe.ID).then(function(response){
    noticia.Detalhe = response.data;
    loadingService.isLoading = false;
  })
}
