module.exports = function noticiasController($scope,dataFactory,loadingService){
	var noticias = this;

	noticias.All = [];

	noticias.title = "Últimas Notícias";

	dataFactory.getUltimasNoticias().then(function(response){
		noticias.All = response.data;
		loadingService.isLoading = false;
	});
}
