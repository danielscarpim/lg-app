import subMenuController from './subMenuController.js';
import template from 'ngtemplate!html!./subMenu.html';

angular
  .module('subMenu',[])
  .controller('subMenuController',subMenuController)
  .component('subMenu',{
    controller: subMenuController,
    contollerAs: 'subMenu',
    templateUrl: template
  })
