module.exports = function($state,$scope,dataFactory,$stateParams,loadingService,uiCalendarConfig){
  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

  var calendario = this;
  calendario.eventos = [];

  var findItem = function(){
    if(calendario.eventos != null)
      return $.grep(calendario.eventos, function(e){ return e.id == id; });

    return null;
  }

  calendario.dayClick = function(date, jsEvent, view) {
    $state.go('main.agenda.turmas', {'date' : date.format()});
  };

  /* Change View */
  calendario.changeView = function(view,calendar) {
    getTurmasByDate(view.intervalStart.format());
  };

  var getTurmasByDate = function(date){
    dataFactory.getTurmasCalendario(date).then(function(response){
      bindCalendarioEventos(response.data);
      loadingService.isLoading = false;
    });
  }

  var bindCalendarioEventos = function(data){
    angular.forEach(data, function(value, key){
      var obj = {};
      obj.id = value.ID;
      obj.title = 'teste';
      obj.start = value.Dia;
      obj.className = ['className'];
      calendario.eventos.push(obj);
    });
  }

  /* config object */
  calendario.uiConfig = {
    calendar:{
      height: 500,
      timezone: 'local',
      header:{
        right: 'prev,today,next'
      },
      defaultView: 'month',
      viewRender: calendario.changeView,
      dayClick: calendario.dayClick,
      buttonText: {
        today: 'Hoje'
      },
      monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    }
  };

  calendario.eventSources = [calendario.eventos];
}
