import calendarioController from './calendarioController.js';
import template from 'ngtemplate!html!./calendario.html';

angular
  .module('calendario',['ui.calendar'])
  .controller('calendarioController',calendarioController)
  .config(calendarioRoutes);

  function calendarioRoutes($stateProvider){
    $stateProvider
      .state('main.agenda.calendario',{
        url: "/calendario",
        views: {
          "tabs-agenda": {
            templateUrl: template,
            controller: "calendarioController",
            controllerAs: "calendario"
          },
        }
      })
  }
