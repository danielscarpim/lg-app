import turmasController from './turmasController.js';
import template from 'ngtemplate!html!./turmas.html';
import turma from './turma/turma.js';

import evidencias from '../evidencias/evidencias.js';
import custos from '../custos/custos.js';

angular
  .module('turmas',['turma', 'evidencias', 'custos'])
  .controller('turmasController',turmasController)
  .config(turmasRoutes);

  function turmasRoutes($stateProvider){
    $stateProvider
      .state('main.agenda.turmas',{
        url: "/turmas/:date",
        data: {
          title: "Turmas",
        },
        views: {
          "tabs-agenda": {
            templateUrl: template,
            controller: "turmasController",
            controllerAs: "turmas"
          },
        }
      })
  }
