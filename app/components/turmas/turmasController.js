module.exports = function turmasController($state,$scope,dataFactory,sharedService,$stateParams,loadingService){
	var turmas = this;

	turmas.selectedDate = $stateParams.date;
	turmas.Kilometragem = [];
	turmas.listaCustos = [];
	turmas.totalDistanceInKM = 0;
	turmas.treinamentos = [];
	turmas.getDatetime = new Date();

	dataFactory.getResumoDia(turmas.selectedDate).then(function(response){
		turmas.Kilometragem = response.data;
		turmas.totalDistanceInKM = calculateDistance(turmas.Kilometragem[0].Positions);
	});

	dataFactory.getListCustosSumario(turmas.selectedDate).then(function(response){
		turmas.listaCustos = response.data;
	});

	dataFactory.getTurmasCalendarioByInstrutorByDia(turmas.selectedDate).then(function(response){
  	turmas.treinamentos = response.data;
  	sharedService.Turmas = turmas.treinamentos;
		loadingService.isLoading = false;
	});

	function calculateDistance(positions){
		if(positions != null){
		var distanceArr = new Array();
		for (var i = 0; i < positions.length; i++) {
                var latLng = new google.maps.LatLng(positions[i].Latitude, positions[i].Longitude);
                distanceArr.push(latLng);
            }

		 var polyline = new google.maps.Polyline({
	        path: distanceArr
	    });

	    var lengthInMeters = google.maps.geometry.spherical.computeLength(polyline.getPath());

		var lengthInKM = (lengthInMeters / 1000).toFixed(2);

		return (1.1 * lengthInKM).toFixed(2);
		}
	}
}
