import turmaController from './turmaController.js';
import template from 'ngtemplate!html!./turma.html';

angular
  .module('turma',[])
  .controller('turmaController',turmaController)
  .config(turmaRoutes);

  function turmaRoutes($stateProvider){
    $stateProvider
      .state('turma',{
        url: "/turma/:id/:date",
        data: {
          title: "turma",
        },
        views: {
          "main": {
            templateUrl: template,
            controller: "turmaController",
            controllerAs: "turma"
          },
        }
      })
  }
