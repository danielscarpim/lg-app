module.exports = function($state,$scope,$interval,$filter,dataFactory, sharedService, $rootScope, $stateParams, loadingService){
	var turma = this;
	turma.Detalhe = sharedService.Turmas;
	turma.Date = $stateParams.date;
	turma.idTurma = $stateParams.id;

	var filterEvento = function(data){
		return $.grep(data, function(e){ return e.ID == parseInt($stateParams.id); });
	}

	if(turma.Detalhe.length != 0){
		//Filter by ID
		turma.Detalhe = sharedService.Turma = filterEvento(turma.Detalhe)[0];
		loadingService.isLoading = false;
	}
	else{
		dataFactory.getTurmaCalendario(turma.idTurma).then(function(response){
    		turma.Detalhe = response.data;
    		loadingService.isLoading = false;
		});
	}
}
