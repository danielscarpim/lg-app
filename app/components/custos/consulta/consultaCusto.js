import consultaCustoController from './consultaCustoController.js';
import template from 'ngtemplate!html!./consultaCusto.html';

angular
  .module('consultaCusto',[])
  .controller('consultaCustoController',consultaCustoController)
  .config(consultaCustoRoutes);

  function consultaCustoRoutes($stateProvider){
    $stateProvider
      .state('consultaCusto',{
        url: "/consultaCusto/:idTurma",
        data: {
          title: "consultaCusto",
        },
        views: {
          "main": {
            templateUrl: template,
            controller: "consultaCustoController",
            controllerAs: "consultaCusto"
          },
        }
      })
  }
