module.exports = function consultaCustoController($state,$scope,dataFactory,sharedService,$stateParams,loadingService){
    var consultaCusto = this;
    consultaCusto.Turma =  sharedService.Turma;
    consultaCusto.idTurma  = $stateParams.idTurma;

    var getCustos = function(){
        return dataFactory.getCustos(consultaCusto.idTurma).then(function(response){
            consultaCusto.All = response.data;
        })
    }

    var loadTurmaCalendario = function(){
        if(consultaCusto.Turma.length == 0){
            dataFactory.getTurmaCalendario(consultaCusto.idTurma).then(function(response){
                consultaCusto.Turma = response.data;
            })
        }
        loadingService.isLoading = false;
    }

    consultaCusto.vinculaCusto = function(idCusto){
        dataFactory.vinculaCusto(consultaCusto.idTurma, idCusto).then(function(response){
            if(response.data.result == true)
                $state.go('custos',  {  "idTurma" : consultaCusto.idTurma });
        })
    }

    getCustos()
    .then(function(){
        loadTurmaCalendario();
    });
}
