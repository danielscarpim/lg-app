import custoController from './custoController.js';
import template from 'ngtemplate!html!./custo.html';

angular
  .module('custo',[])
  .controller('custoController',custoController)
  .config(custoRoutes);

  function custoRoutes($stateProvider){
    $stateProvider
      .state('custo',{
        url: "/custo/:idTurma/:idCusto/:date/:status",
        data: {
          title: "custo",
        },
        views: {
          "main": {
            templateUrl: template,
            controller: "custoController",
            controllerAs: "custo"
          },
        }
      })
  }
