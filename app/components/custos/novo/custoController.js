module.exports = function custoController($state,$scope,dataFactory,sharedService,$stateParams,loadingService){
    var custo = this;
    
    custo.Turma =  sharedService.Turma;
    custo.Data = {};

    custo.Status = $stateParams.status;
    custo.idTurma = custo.Data.idTurma = $stateParams.idTurma;
    custo.idCusto = $stateParams.idCusto; 
    custo.date = $stateParams.date;

    custo.InitialDate = getCurrentDate();
    custo.TipoCusto = {}

    dataFactory.getTiposCusto().then(function(response){
        custo.TiposCusto = response.data;
        custo.Data.TipoCusto = custo.TiposCusto[0];
    })
    .then(function(){
        loadCustoById();
    })
    .then(function(){
        loadTurmaCalendario();
    });

    var loadTurmaCalendario = function(){
            if(custo.Turma.length == 0){
                dataFactory.getTurmaCalendario(custo.idTurma).then(function(response){
                    custo.Turma = response.data;    
            })
        }
        loadingService.isLoading = false;
    }

    var loadCustoById = function(){
        if(custo.idCusto != 0){
            dataFactory.getCusto(custo.idCusto).then(function(response){
                bindCusto(response.data);
            })
        }
    }

    var bindCusto = function(data){
        var date = new Date(data.Data.match(/\d+/)[0] * 1);
        date = new Date(date);
        custo.Data.ID = data.ID;
        custo.Data.TipoCusto = data.TipoCusto.ID;
        custo.Data.Date = date;
        custo.Data.Custo = data.Custo;
        custo.Data.Fornecedor = data.Fornecedor;
        custo.Data.NF = data.NF;
        custo.Data.Descricao = data.Descricao;
        custo.InitialDate = getCurrentDate(date);
        custo.Data.Anexos = data.Anexos;
    }

    custo.NovoCusto = function(custo, status){
        if(status != "edit"){
                var newcusto = {
                IDTurma: custo.idTurma,
                Custo: {
                    ID: custo.ID,
                    Fornecedor: custo.Fornecedor,
                    NF: custo.NF,
                    Descricao: custo.Descricao,
                    TipoCusto: { ID : custo.TipoCusto },
                    Data: custo.Date,
                    Custo: custo.Custo,
                    Anexos: [{ Arquivo: custo.FileName != undefined ? custo.FileName : '', Path: custo.FileData != undefined ? custo.FileData : ''}]
                }
            }

            //Init request service to Upload NOVO RECIBO
            try{
                dataFactory.salvaNovoCusto(newcusto).then(function(response){
                    if(response.data == null || response.data.d == null) {
                        loadingService.isLoading = false;
                        return;
                    }

                    var result = angular.fromJson(response.data.d).result;
                   
                    if(result == true)
                        $state.go('custos',  {  "idTurma" : custo.idTurma });
                });
            }
            catch (err) {
                console.log(err)
            }
        }else{
            var editcusto = {
                ID: custo.ID,
                Fornecedor: custo.Fornecedor,
                NF: custo.NF,
                Descricao: custo.Descricao,
                TipoCusto: { ID : custo.TipoCusto },
                Data: custo.Date,
                Custo: custo.Custo,
                Anexos: [{ Arquivo: custo.FileName != undefined ? custo.FileName : '', Path: custo.FileData != undefined ? custo.FileData : ''}]
            }

            try{
                //Execute when edit custo
                dataFactory.uploadRecibo(editcusto).then(function(response){
                    if(response.data == null) 
                        loadingService.isLoading = false;

                    var result = angular.fromJson(response.data.d).result;

                    if(result == true)
                        $state.go('custos',  {  "idTurma" : custo.idTurma });
                });
            }
            catch (err) {
                console.log(err)
            }
        }
    }

    custo.uploadRecibo = function(custo, status){
        var editcusto = {
                ID: custo.ID,
                Fornecedor: custo.Fornecedor,
                NF: custo.NF,
                Descricao: custo.Descricao,
                TipoCusto: { ID : custo.TipoCusto },
                Data: custo.Date,
                Custo: custo.Custo,
                Anexos: [{ Arquivo: custo.FileName != undefined ? custo.FileName : '', Path: custo.FileData != undefined ? custo.FileData : ''}]
            }

            try{
                //Execute when edit custo
                dataFactory.uploadRecibo(editcusto).then(function(response){
                    if(response.data == null) 
                        loadingService.isLoading = false;

                    var result = angular.fromJson(response.data.d);

                    if(result.status == "error"){
                        console.log(result.message);
                        return;
                    }

                    if(result.status == true)
                        $state.reload();
                });
            }
            catch (err) {
                console.log(err)
            }
    }

    $scope.uploadImage = function (files) {
        if(files[0] != undefined || files[0] != ''){
            var name = files[0].name;
             
            var reader = new FileReader();

            reader.onload = function(e){
                var data = e.currentTarget.result.replace('data:base64,','data:image\/'+name.split(".")[1] + ';base64,');
                
                sharedService.resizeBase64Img(data, null).then(function(newImg){
                    var data = newImg.split(",");
                    
                    var file  = {};
                    file.id   = custo.idTurma;
                    file.data = data[1];
                    file.name = name;

                    try {
                        custo.Data.FileName = file.name;
                        custo.Data.FileData = file.data;

                        custo.uploadRecibo(custo.Data, custo.Status);
                        loadingService.isLoading = false;
                    }
                    catch (err) {
                        console.log(err);
                    }
               });
                   
            };
            reader.readAsDataURL(files[0]);
        }
    }

    function getCurrentDate(date){
        var today = {};
        if(date == undefined)
            today = new Date();
        else
            today = date;

        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        return today = yyyy+'/'+mm+'/'+dd;
    }

    custo.DeleteRecibo = function(){
        dataFactory.DeleteRecibo(custo.idCusto, custo.Data.Anexos[0].Arquivo).then(function(response){
            if(response.data.result == true){
                loadCustoById();
            }
            loadingService.isLoading = false;
        })
    }
}