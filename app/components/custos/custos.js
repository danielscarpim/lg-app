import custosController from './custosController.js';
import template from 'ngtemplate!html!./custos.html';

import custo from './novo/custo.js';
import consultaCusto from './consulta/consultaCusto.js';

angular
  .module('custos',['custo', 'consultaCusto'])
  .controller('custosController',custosController)
  .config(custosRoutes);

  function custosRoutes($stateProvider){
    $stateProvider
      .state('custos',{
        url: "/custos/:idTurma/:date",
        data: {
          title: "custos",
        },
        views: {
          "main": {
            templateUrl: template,
            controller: "custosController",
            controllerAs: "custos"
          },
        }
      })
  }
