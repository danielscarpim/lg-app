module.exports = function custosController($state,$scope,dataFactory,sharedService,$stateParams,loadingService){
	var custos = this;

	custos.Turma = sharedService.Turma;
	custos.idTurma = $stateParams.idTurma;
	custos.date = $stateParams.date;

	custos.deleteCusto = function(idCusto){
		dataFactory.deleteCusto(custos.idTurma, idCusto).then(function(response){
			if(response.data.result == true){
				getCustosByTurma();
				loadingService.isLoading = false;
			}
		})
	}

	var getCustosByTurma = function(){
		return dataFactory.getCustosByTurma(custos.idTurma).then(function(response){
			custos.All = response.data;
		})
	}

	var loadTurmaCalendario = function(){
		if(custos.Turma.length == 0 || custos.Turma == undefined){
			dataFactory.getTurmaCalendario(custos.idTurma).then(function(response){
				custos.Turma = response.data;
			})
		}
		loadingService.isLoading = false;
	}

	getCustosByTurma()
	.then(function(){
		loadTurmaCalendario();
	})
}
