import evidenciasController from './evidenciasController.js';
import template from 'ngtemplate!html!./evidencias.html';

angular
  .module('evidencias',[])
  .controller('evidenciasController',evidenciasController)
  .config(evidenciasRoutes);

  function evidenciasRoutes($stateProvider){
    $stateProvider
      .state('evidencias',{
        url: "/evidencias/:idTurma/:date",
        data: {
          title: "evidencias",
        },
        views: {
          "main": {
            templateUrl: template,
            controller: "evidenciasController",
            controllerAs: "evidencias"
          },
        }
      })
  }
