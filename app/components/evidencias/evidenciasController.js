module.exports = function turmasController($state,$scope,dataFactory,sharedService,$stateParams,loadingService){
	var evidencias = this;

	evidencias.Turma = sharedService.Turma;
	evidencias.idTurma = $stateParams.idTurma;
	evidencias.date = $stateParams.date;
	evidencias.uploadFile = {};
	evidencias.fileName = {};

	var getEvidencias = function(){
			dataFactory.getEvidencias(evidencias.idTurma).then(function(response){
			evidencias.Contents = response.data;
			loadingService.isLoading = false;
		})
	}

	var getTurmaCalendario = function(){
		if(evidencias.Turma.length == 0){
			dataFactory.getTurmaCalendario(evidencias.idTurma).then(function(response){
				evidencias.Turma = response.data;
				loadingService.isLoading = false;
			})
		}
	}

	evidencias.deleteEvidencia = function(path){
		var splittedSpath = path.split('/');
		var filePath = splittedSpath[splittedSpath.length -1];

		dataFactory.deleteEvidencia(evidencias.idTurma, filePath).then(function(response){
			loadingService.isLoading = false;
			if(response.data.status == true)
				getEvidencias();
		})
	}

	getEvidencias();
	getTurmaCalendario();

 	$scope.uploadImage = function (files) {
        if(files[0] != undefined || files[0] != ''){
        	var name = files[0].name;
			var reader = new FileReader();

			reader.onload = function(e){
				var data = e.currentTarget.result.replace('data:base64,','data:image\/'+name.split(".")[1] + ';base64,');

				sharedService.resizeBase64Img(data, 'imgEvidencia').then(function(newImg){

					var data = newImg.split(",");

					var file  = {};
					file.id   = evidencias.idTurma;
					file.data = data[1];
					file.name = name;

	                try {
		                   dataFactory.putNewEvidencia(file).then(function(response){
		                   		getEvidencias();
		                   }, function errorCallback(response) {
							    console.log(response);
								loadingService.isLoading = false;
							});
	                }
	                catch (err) {
	                	console.log(err);
            		}
               });
		    };
		    reader.readAsDataURL(files[0]);
        }
    }
}
