import template from 'ngtemplate!html!./pageHeader.html';
import pageHeaderController from './pageHeaderController.js';

angular
  .module('pageHeader',[])
  .component('pageHeader',{
    transclude: true,
    templateUrl: template,
    controller: pageHeaderController
  })
