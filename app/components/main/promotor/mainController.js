module.exports = function mainController($scope,$state,sharedService){
  sharedService.pages = {
		menu: [
    {
      name: 'Treinamentos',
      path: 'main.treinamentos'
    },{
      name: 'Biblioteca',
      path: 'main.biblioteca'
    },{
      name: 'Avaliações',
      path: 'main.avaliacoes'
    }],
		submenu: [
    {
      name: 'Pré Teste',
      path: 'main.avaliacoes({tipoAvaliacao: "PRE"})',
      icon: 'ion-log-in'
    },{
      name: 'Pós Teste',
      path: 'main.avaliacoes({tipoAvaliacao: "POS"})',
      icon: 'ion-log-out'
    }]
	}
}
