module.exports = function mainController($scope,$state,sharedService){
  sharedService.pages = {
		menu: [
    {
      name: 'Agenda',
      path: 'main.agenda'
    },{
      name: 'Notícias',
      path: 'main.noticias'
    }],
		submenu: [
    {
      name: 'Calendário',
      path: 'main.agenda.calendario',
      icon: 'ion-android-calendar'
    },{
      name: 'Turmas',
      path: 'main.agenda.turmas',
      icon: 'ion-navicon'
    }]
	}
}
