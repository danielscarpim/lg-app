import mainController from './mainController.js';
import template from 'ngtemplate!html!./main.html';

import menu from '../../../components/menu/menu.js';
import topBar from '../../../components/topBar/topBar.js';
import loader from '../../../components/loader/loader.js';


angular
  .module('main.instrutor',['topBar','menu','loader'])
  .controller('mainController',mainController)
  .config(mainRoutes);

  function mainRoutes($stateProvider,$urlRouterProvider){
    $urlRouterProvider.otherwise("/main");
    $urlRouterProvider.when('/main', '/main/agenda');
    $stateProvider
      .state('main',{
        url: "/main",
        views: {
          "main": {
            templateUrl: template,
            controller: "mainController"
          },
        }
      })
  }
