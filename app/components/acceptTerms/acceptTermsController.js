module.exports = function acceptTermsController(sharedService,$scope,$state,$stateParams,dataFactory){

  this.status = sharedService;
  this.tipoAvaliacao = $stateParams.tipoAvaliacao;

  dataFactory.getTermoAceiteAvaliacao().then(function(response){
    if(response.data.Aceite === false)
      sharedService.acceptTermsStatus = false;
    else
      sharedService.acceptTermsStatus = true;
  })


  this.confirm = function(){
    sharedService.acceptTermsStatus = true;
    dataFactory.putTermoAceiteAvaliacao(true);
  }

  this.deny = function(){
    sharedService.acceptTermsStatus = false;
    dataFactory.putTermoAceiteAvaliacao(false);
    $state.go('main.avaliacoes', {'tipoAvaliacao': this.tipoAvaliacao});
  }
}
