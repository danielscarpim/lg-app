import acceptTermsController from './acceptTermsController.js';
import template from 'ngtemplate!html!./acceptTerms.html';

angular
  .module('acceptTerms',[])
  .component('acceptTerms',{
    templateUrl: template,
    controller: acceptTermsController
  })
