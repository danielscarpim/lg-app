import menuController from './menuController.js';
import template from 'ngtemplate!html!./menu.html';

angular
  .module('menu',[])
  .controller('menuController',menuController)
  .component('menu',{
    controller: menuController,
    contollerAs: 'menu',
    templateUrl: template
  })
