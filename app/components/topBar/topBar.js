import template from 'ngtemplate!html!./topBar.html';

angular
  .module('topBar',[])
  .component('topBar',{
    templateUrl: template
  })
