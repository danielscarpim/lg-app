'use strict';

// Modules
var path = require('path')
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
      app:'./app/app.js',
      lib: './app/lib.js'
    },
    output: {
        path: 'dist',
        filename: 'js/[name].bundle.js'
    },
    devServer: {
      contentBase: './dist',
      stats: 'minimal',
	  port: 8000
    },
    devtool: 'source-map',
    module: {
      loaders:[{
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
              presets: ['es2015']
            }
      },{
        test: /\.scss$/,
        exclude: [/node_modules/],
        loader: ExtractTextPlugin.extract(
          'style',
          'css?sourceMap!sass?sourceMap')
      },{
        test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
        loader: 'url?limit=100000&name=fonts/[name].[ext]'
      },{
        test: /\.(png|jpe?g|gif)(\?\S*)?$/,
        loader: 'url?limit=100000&name=images/[name].[ext]'
      }]
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: "LG App",
        filename: 'index.html',
        favicon: './app/images/favicon.png',
        template: './app/index.html',
        inject: 'body'
      }),
      new HtmlWebpackPlugin({
        title: "LG App",
        filename: 'promotor.html',
        favicon: './app/images/favicon.png',
        template: './app/promotor.html',
        inject: 'body'
      }),
      new ExtractTextPlugin("[name].css"),
      new webpack.ProvidePlugin({
            '$': 'jquery',
            'window.jQuery': 'jquery',
            'window.$': 'jquery',
            'moment': 'moment',
            'Offline': 'offline',
            'offline': 'offline'
        })
    ],
    sassLoader: {
      includePaths: [path.resolve(__dirname, "node_modules")]
    }
};
