var webpackConfig = require('./webpack.config.js');
webpackConfig.entry = {};

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],

    reporters: ['spec'],
    port: 9876,
    colors: false,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './dist/js/app.js',
      './node_modules/angular/angular.min.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './app/**/*.spec.js'],

    preprocessors: {
      './dist/js/app.js': ['webpack'],
      './app/**/*.spec.js': ['babel']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true
    }
  });
}
